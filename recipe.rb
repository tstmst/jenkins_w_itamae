execute 'install jenkins add apt-key' do
  command <<-COMMANDS
#!/bin/bash
wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install jenkins -y
  COMMANDS
  not_if 'test -e /etc/apt/sources.list.d/jenkins.list'
end

execute 'sudo service jenkins restart' do
  subscribes :run, 'file[/etc/default/jenkins]'
  action :nothing
end

file '/etc/default/jenkins' do
  action :edit
  block do |content|
    content.gsub!(/^HTTP_PORT=.+.*$/, %q(HTTP_PORT=9090))
  end
end

service 'jenkins' do
  action [:enable, :start]
end


execute 'intall node.js v6.x' do
  command <<-COMMANDS
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
  COMMANDS
  not_if 'node -v'
end
